import React from "react";
import { useDispatch } from "react-redux";
import Column from "./column";

const moveCard = dispatchFn => (fromCol, toCol) => (cardText, cardIndex) => {
  dispatchFn({
    type: "addCard",
    payload: {
      colIndex: toCol,
      card: cardText
    }
  });
  dispatchFn({
    type: "removeCard",
    payload: {
      colIndex: fromCol,
      cardIndex
    }
  });
};

export default () => {
  const move = moveCard(useDispatch());
  return (
    <div className="board">
      <Column colIndex={0} bgColor="#8E6E95" onRightArrow={move(0, 1)} />
      <Column
        colIndex={1}
        bgColor="#39A59C"
        onLeftArrow={move(1, 0)}
        onRightArrow={move(1, 2)}
      />
      <Column
        colIndex={2}
        bgColor="#344759"
        onLeftArrow={move(2, 1)}
        onRightArrow={move(2, 3)}
      />
      <Column colIndex={3} bgColor="#E8741E" onLeftArrow={move(3, 2)} />
    </div>
  );
};
