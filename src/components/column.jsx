import { mdiChevronLeft, mdiChevronRight, mdiPlusCircle } from "@mdi/js";
import { Icon } from "@mdi/react";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { columnSelector } from "../store";

export default props => {
  const colData = useSelector(columnSelector(props.colIndex));
  const dispatch = useDispatch();

  return (
    <div className="column">
      <div className="column-title" style={{ backgroundColor: props.bgColor }}>
        {colData.name}
      </div>
      {colData.cards.map((card, cardIndex) => (
        <div className="card" key={cardIndex}>
          {props.colIndex > 0 ? (
            <a
              href="#"
              onClick={() => {
                props.onLeftArrow(card, cardIndex);
              }}
            >
              <Icon path={mdiChevronLeft} size={1} />
            </a>
          ) : (
            <div className="dummy" />
          )}
          <div className="card-text">{card}</div>
          {props.colIndex < 3 ? (
            <a
              href="#"
              onClick={() => {
                props.onRightArrow(card, cardIndex);
              }}
            >
              <Icon path={mdiChevronRight} size={1} />
            </a>
          ) : (
            <div className="dummy" />
          )}
        </div>
      ))}
      <div className="add-card">
        <button
          type="button"
          onClick={() =>
            dispatch({
              type: "addCard",
              payload: {
                colIndex: props.colIndex,
                card: window.prompt("Card text:")
              }
            })
          }
        >
          <Icon path={mdiPlusCircle} size={1} color="white"/>
          Add a card
        </button>
      </div>
    </div>
  );
};
