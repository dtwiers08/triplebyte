import { createStore } from "redux";

const initialState = [
  {
    name: "col1",
    cards: ["card1", "card2"]
  },
  {
    name: "col1",
    cards: ["card3", "card4"]
  },
  {
    name: "col1",
    cards: ["card5", "card6"]
  },
  {
    name: "col1",
    cards: ["card7", "card8"]
  }
];

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "addCard": {
      return state.map((col, colIndex) =>
        colIndex === action.payload.colIndex
          ? { ...col, cards: [...col.cards, action.payload.card] }
          : col
      );
    }
    case "removeCard": {
      return state.map((col, colIndex) =>
        colIndex === action.payload.colIndex
          ? {
              ...col,
              cards: col.cards.filter((_, i) => i !== action.payload.cardIndex)
            }
          : col
      );
    }
    default:
      return state;
  }
};

export const store = () => createStore(reducer);

export const columnSelector = col => state => {
  return state ? state[col] : state;
};

export const storeSelector = state => state;
